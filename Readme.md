GET  - return values
POST - sending values to server
PUT  - adding new value
PATCH - updating new value
DELETE - delete new

GET /user/:id - /user/1234 
POST /user [{name, email, id},{name, email, id}]  - create new user
PUT /user {name, email, id} - create one user
PATCH /user { address, email } - modify one user
DELETE /user/:id - /user/1234 - Remove user


Path
PathVariable
RequestBody
QueryString

@Bean - Annotation is used to create object
@Scope - Annoation is used to tell Spring to create an object in design patterns like
        * Singleton
        * Prototype
        ---- Spring MVC ---
        * Request
        * Session
        * Global
        
@Autowired - Pull object from Spring Application context and assign to a variable

@Component - Its just a wrapper of @Bean
@Service   - Its wrapper of Component
@Repository - Its Wrapper of Component & exception
        
        