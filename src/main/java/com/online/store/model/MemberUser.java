package com.online.store.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class MemberUser extends User{

    public MemberUser(String firstName, String lastName, String userId){
        super(firstName, lastName, userId);
    }

    private MembershipType membershipType;

    private Date expiresOn;

}


