package com.online.store.model;

public enum MembershipType {
    MONTHLY,
    SEMI_MONTHLY,
    ANNUALLY
}
