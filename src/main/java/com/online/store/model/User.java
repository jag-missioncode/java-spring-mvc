package com.online.store.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
public abstract class User {

    private String firstName;
    private String lastName;
    private String userId;

}
