package com.online.store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource("classpath*:applicationContext.xml")
public class GroceryStoreApplication {
    public static void main(String[] args) {
        SpringApplication.run(GroceryStoreApplication.class, args);
    }
}
