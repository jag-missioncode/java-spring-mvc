package com.online.store.config;

import com.online.store.annotations.Prod;
import com.online.store.repository.PersonRepositoryImpl;
import com.online.store.repository.UserRepository;
import com.online.store.repository.UserRepositoryImpl;
import com.online.store.service.UserService;
import com.online.store.service.UserServiceImpl;
import org.springframework.context.annotation.*;

@EnableAspectJAutoProxy
@Configuration
@ComponentScan("com.online.store")
@Prod
public class ProdAppConfig {

    /*@Bean(name = "userService")
    public UserService getPersonService(){
        UserRepository userRepository = getPersonRepository();
        return new UserServiceImpl(userRepository);
    }

    @Bean(name = "userRepository")
    public UserRepository getPersonRepository(){
        return new PersonRepositoryImpl();
    }*/

}
