package com.online.store.config;

import com.online.store.annotations.NonProd;
import com.online.store.annotations.Prod;
import com.online.store.repository.PersonRepositoryImpl;
import com.online.store.repository.UserRepository;
import com.online.store.repository.UserRepositoryImpl;
import com.online.store.service.UserService;
import com.online.store.service.UserServiceImpl;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.*;

@EnableAspectJAutoProxy
@Configuration
@ComponentScan("com.online.store")
@NonProd
public class AppConfig {

    @Bean(name = "userService")
    public UserService getUserService(){
        UserRepository userRepository = getUserRepository();
        return new UserServiceImpl(userRepository);
    }

    @Bean(name = "userRepository")
    public UserRepository getUserRepository(){
        return new UserRepositoryImpl();
    }

}
