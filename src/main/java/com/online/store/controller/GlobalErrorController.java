package com.online.store.controller;

import com.online.store.model.ErrorResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

@Controller
public class GlobalErrorController implements ErrorController {

    @Value("${path.not.found}")
    private String errorMessage;

    @RequestMapping("/error")
    @ResponseBody
    public ErrorResponse handleError(HttpServletResponse response){
        return new ErrorResponse(response.getStatus(), errorMessage);
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
