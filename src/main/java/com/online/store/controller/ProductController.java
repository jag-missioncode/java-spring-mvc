package com.online.store.controller;

import com.online.store.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/product")
public class ProductController {
    // Bean LifeCyle
    // Constructor -> PostContruct -> PreDestory -> Destroy
    @Autowired
    private ApplicationContext applicationContext;

    private ProductService productService;

    @Value("#{ T(java.lang.Math).random() * 10000}")
    private double uniqueId;

    @Value("#{ T(java.util.UUID).randomUUID() }")
    private UUID uuid;

    @PostConstruct
    private void init(){
        this.productService = applicationContext.getBean("productService", ProductService.class);
        // uniqueId = Math.random()*10;
        // UUID uuid = UUID.randomUUID();

        System.out.println("Unique ID : " + uniqueId);
        System.out.println("UUID : " + uuid);
    }

    @PreDestroy
    private void destroy(){
        this.productService = null;
    }

    @GetMapping("/all")
    public List<String> allProducts(){
        return this.productService.findAll();
    }

    @PostMapping("/add")
    public List<String> addProducts(){
        return this.productService.findAll();
    }

}
