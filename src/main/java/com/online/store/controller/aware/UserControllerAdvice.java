package com.online.store.controller.aware;

import com.online.store.exceptions.UserNotFoundException;
import com.online.store.model.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
@Slf4j
public class UserControllerAdvice {

    @ExceptionHandler(UserNotFoundException.class)
    @ResponseBody
    public ErrorResponse handleUserNotFound(UserNotFoundException userNotFoundException){
        log.info("handle userNotFound exception");
        return  new ErrorResponse(HttpStatus.NOT_FOUND.value(), userNotFoundException.getMessage());
    }

}
