package com.online.store.controller;

import com.online.store.annotations.Trace;
import com.online.store.exceptions.UserNotFoundException;
import com.online.store.model.User;
import com.online.store.service.UserService;
import com.online.store.service.UserServiceImpl;
import com.sun.org.apache.bcel.internal.generic.PUTFIELD;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
//public class UserController implements ApplicationContextAware {
public class UserController {

    @Autowired
    Environment environment;

    @Autowired
    private UserService userService;

    public UserController(){
        // environment.getProperty("user.service.url");

        //this.userService = new UserServiceImpl();
    }

    @GetMapping("/all")
    @Trace
    public List<User> getUsers(){
        return this.userService.findAll();

    }

    @GetMapping("/{userId}")
    public User getUser(@PathVariable String userId){
        return this.userService.findById(userId);
    }

    @PutMapping("/add")
    public User addUser(@RequestBody User user){
        return null;
    }

    /*@Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.userService = (UserService) applicationContext.getBean("userServices");
    }*/
}
