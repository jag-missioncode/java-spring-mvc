package com.online.store.service;

import com.online.store.model.User;

import java.util.List;

public interface UserService {

    List<User> findAll();

    default User findById(String userId){
        return null;
    }

}
