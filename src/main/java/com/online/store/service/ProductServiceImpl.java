package com.online.store.service;

import com.online.store.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class ProductServiceImpl implements ProductService {

    // private String type = "REGULAR";

    private ProductRepository productRepository;

    public void setProductRepository(ProductRepository productRepository) {
        System.out.println("Setter Injection byName");
        this.productRepository = productRepository;
    }

    public void setRepository(ProductRepository productRepository) {
        System.out.println("Setter Injection byType");
        this.productRepository = productRepository;
    }

    public ProductServiceImpl(){
        System.out.println("ProductServiceImpl constructor");
    }

    // Constructor Injection
    public ProductServiceImpl(ProductRepository productRepository) {
        System.out.println("ProductServiceImpl constructor with args");
        this.productRepository = productRepository;
    }

    @Override
    public List<String> findAll(){
        return this.productRepository.findAll();
    }
}
