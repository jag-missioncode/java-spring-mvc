package com.online.store.service;

import java.util.List;

public interface ProductService {
    List<String> findAll();
}
