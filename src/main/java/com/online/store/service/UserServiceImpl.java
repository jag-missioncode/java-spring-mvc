package com.online.store.service;

import com.online.store.model.User;
import com.online.store.repository.UserRepository;
import com.online.store.repository.UserRepositoryImpl;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private String name;

    @NonNull
    private UserRepository userRepository;

    @Override
    public List<User> findAll(){
        return this.userRepository.findAll();
    }

    @Override
    public User findById(String userId){
        return this.userRepository.findById(userId);
    }

    /*
    public UserServiceImpl(){
        System.out.println("UserService Constructor without arguments");
        this.userRepository = new UserRepositoryImpl();
    }

    public void setUserRepository(UserRepository userRepository){
        System.out.println("UserRepository Setter");
        this.userRepository = userRepository;
    }*/

}
