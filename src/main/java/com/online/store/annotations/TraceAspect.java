package com.online.store.annotations;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TraceAspect {

    // Pointcut - Around
    // PointCut - Before
    @Around("@annotation(Trace)")
    public Object traceIt(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.printf("%s ", joinPoint.getSignature().getName());
        return joinPoint.proceed();
    }

    @Around("@annotation(Repository)")
    public Object handleTransactionException(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.printf("%s ", joinPoint.getSignature().getName());
        return joinPoint.proceed();
    }
}
