package com.online.store.annotations;

import org.springframework.context.annotation.Bean;

@Bean
public @interface Component {
}
