package com.online.store.repository;

import java.util.Arrays;
import java.util.List;

public class ProductRepositoryImpl implements ProductRepository {

    public ProductRepositoryImpl() {
        System.out.println("ProductRepositoryImpl Constructor");
    }

    @Override
    public List<String> findAll(){
        return Arrays.asList("Product1, Product2");
    }

}
