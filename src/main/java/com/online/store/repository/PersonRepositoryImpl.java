package com.online.store.repository;

import com.online.store.model.User;

import java.util.ArrayList;
import java.util.List;

public class PersonRepositoryImpl implements UserRepository{

    public PersonRepositoryImpl(){
        System.out.println("PersonRepositoryImpl Constructor without args");
    }

    private List<User> users;

    @Override
    public List<User> findAll(){
        this.users = new ArrayList<>();
        // this.users.add("Person1");
        // this.users.add("Person2");
        return this.users;
    }
}
