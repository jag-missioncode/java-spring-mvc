package com.online.store.repository;

import com.online.store.model.User;

import java.util.List;

public interface UserRepository {
    List<User> findAll();

    default User findById(String userId){
        return null;
    }
}
