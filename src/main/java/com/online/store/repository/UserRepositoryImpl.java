package com.online.store.repository;

import com.online.store.exceptions.UserNotFoundException;
import com.online.store.model.MemberUser;
import com.online.store.model.User;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
@NoArgsConstructor
public class UserRepositoryImpl implements UserRepository {

    private List<User> users;

    @PostConstruct
    private void mockUsers(){
        this.users = new ArrayList<>();
        this.users.add(new MemberUser("Jagadesh", "Kannan", "jkannan"));
        this.users.add(new MemberUser("Sai", "Gayathri", "saigayathri"));
        this.users.add(new MemberUser("Hari", "babu", "hbabu"));
    }

    @Override
    public List<User> findAll(){
        return this.users;
    }

    @Override
    public User findById(String userId){
        Optional<User> optionalUser = this.users
                .stream()
                .filter(user -> user.getUserId().equalsIgnoreCase(userId))
                .findFirst();

        if(optionalUser.isPresent()){
            return optionalUser.get();
        }
        throw new UserNotFoundException("User is not found");
    }

}
