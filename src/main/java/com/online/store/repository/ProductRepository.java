package com.online.store.repository;

import java.util.List;

public interface ProductRepository {
    List<String> findAll();
}
